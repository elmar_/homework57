import React, {useState} from 'react';
import Count from "../../components/Count/Count";

const Mode1 = () => {
    const [order, setOrder] = useState({number: '', price: '', tip: '', delivery: ''});
    const [count, setCount] = useState({});


    const onChangeNumber = e => {
      const numberCopy = {...order};
      numberCopy.number = parseInt(e.target.value);

      if (numberCopy.number <= 0) alert('Неправильно ввели данные');
        else setOrder(numberCopy);
    };

    const onChangePrice = e => {
      const priceCopy = {...order};
      priceCopy.price = parseInt(e.target.value);

      if (priceCopy.price <= 0) alert('Неправильно ввели данные');
        else setOrder(priceCopy);
    };

    const onChangeTip = e => {
      const tipCopy = {...order};
      tipCopy.tip = parseInt(e.target.value);

      if (tipCopy.tip <= 0) alert('Неправильно ввели данные');
        else setOrder(tipCopy);
    };

    const onChangeDelivery = e => {
        const deliveryCopy = {...order};
        deliveryCopy.delivery = parseInt(e.target.value);
        if (isNaN(deliveryCopy.delivery)) deliveryCopy.delivery = 0;

        if (deliveryCopy.delivery < 0) alert('Неправильно ввели данные');
            else setOrder(deliveryCopy);
    };

    const countOrder = () => {
        const sum = order.price + (order.tip / 100 * order.price) + order.delivery;
        const personPay = Math.ceil(sum / order.number);

        setCount({sum: sum, pay: personPay, person: order.number});
    };


    return (
        <div>
            <p>Человек: <input type="number" value={order.number} onChange={e => onChangeNumber(e)} /> чел</p>
            <p>Сумма заказа: <input type="number" value={order.price} onChange={e => onChangePrice(e)} /> сом</p>
            <p>Процент чаевых: <input type="number" value={order.tip} onChange={e => onChangeTip(e)} /> %</p>
            <p>Доставка: <input type="number" value={order.delivery} onChange={e => onChangeDelivery(e)} /> сом</p>
            <button type="button" onClick={countOrder}>Рассчитать</button>
            <Count data={count} />
        </div>
    );
};

export default Mode1;