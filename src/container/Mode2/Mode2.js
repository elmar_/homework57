import React, {useState} from 'react';
import './Mode2.css';
import Count2 from "../../components/Count2/Count2";

const Mode2 = () => {
    const [people, setPeople] = useState([{name: '', price: ''}]);

    const [count, setCount] = useState({});

    const BEFORE_STATE = {total: 0, tip: 0, delivery: 0};

    const onNameChange = (e, index) => {
        const peopleCopy = [...people];
        const personCopy = peopleCopy[index];
        personCopy.name = e.target.value;
        peopleCopy[index] = personCopy;

        setPeople(peopleCopy);
    };

    const onPriceChange = (e, index) => {
        const peopleCopy = [...people];
        const personCopy = peopleCopy[index];
        personCopy.price = parseInt(e.target.value);
        peopleCopy[index] = personCopy;

        setPeople(peopleCopy);
    };

    const removePerson = index => {
        const peopleCopy = [...people];
        peopleCopy.splice(index, 1);

        setPeople(peopleCopy);
    };

    const addPerson = () => {
        setPeople([...people, {}]);
    };

    const addTip = e => {
      BEFORE_STATE.tip = parseInt(e.target.value);
    };

    const addDelivery = e => {
      if (isNaN(e.target.value)) BEFORE_STATE.delivery = 0;
        else BEFORE_STATE.delivery = parseInt(e.target.value);
    };

    const finalOrder = () => {
      const sum = people.reduce((total, person) => total + person.price, 0);
      const total = sum + sum * BEFORE_STATE.tip / 100 + BEFORE_STATE.delivery;
      BEFORE_STATE.total = Math.ceil(total);

      setCount(BEFORE_STATE);
    };


    return (
        <div>
            Mode-2
            <div>
                {people.map((person, index) => (
                    <p>
                        <input type="text" className="name" value={person.name} onChange={e => onNameChange(e, index)} />
                        <input type="number" className="number" value={person.price} onChange={e => onPriceChange(e, index)} /> сом
                        <button type="button" className="remove-btn" onClick={() => removePerson(index)}>Удалить</button>
                    </p>
                ))}
            </div>
            <button type="button" onClick={addPerson} className="add">Добавить</button>
            <div className="tip">Процент чаевых: <input type="number" onChange={e => addTip(e)} /></div>
            <div className="delivery">Доставка: <input type="number" onChange={e => addDelivery(e)} /></div>
            <button type="button" onClick={finalOrder}>Рассчитать</button>
            <Count2 people={people} count={count} />
        </div>
    );
};

export default Mode2;