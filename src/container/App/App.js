import './App.css';
import React, {useState} from "react";
import Mode1 from "../Mode1/Mode1";
import Mode2 from "../Mode2/Mode2";

const App = () => {

    const [mode, setMode] = useState('mode-1');

    const onRadioChange = e => {
        setMode(e.target.value);
    }

  return (
    <div className="App">
      <p>
        <input
            type="radio"
            name="mode"
            id="mode-1"
            value="mode-1"
            onChange={onRadioChange}
            checked={mode === 'mode-1'}
        />
        <label htmlFor="mode-1">mode-1</label>
      </p>
      <p>
        <input
            type="radio"
            name="mode"
            id="mode-2"
            value="mode-2"
            onChange={onRadioChange}
            checked={mode === 'mode-2'}
        />
        <label htmlFor="mode-2">mode-2</label>
      </p>
        {mode === 'mode-1' ? <Mode1 /> : <Mode2 />}
    </div>
  );
}

export default App;
