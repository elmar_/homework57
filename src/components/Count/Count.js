import React from 'react';

const Count = ({data}) => {
    return (
        <div style={{display: (data.sum>0) ? 'block' : 'none'}}>
            <p>Общая сумма: {data.sum}</p>
            <p>Количество человек: {data.person}</p>
            <p>Каждый пдатит: {data.pay}</p>
        </div>
    );
};

export default Count;