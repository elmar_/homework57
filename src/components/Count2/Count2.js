import React from 'react';

const Count2 = ({people, count}) => {
    return (
        <div style={{display: (count.total > 0) ? "block" : "none"}}>
            <p>Общая сумма: {count.total}</p>
            {people.map(person => (
                <p>{person.name}: {person.price} сом</p>
            ))}
        </div>
    );
};

export default Count2;